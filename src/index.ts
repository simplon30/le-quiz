//Pour importer les images grace à : global.d.ts / declare module '*.jpg';
import laNuitEtoilee from '../img/laNuitEtoilee.jpg'
import Vermeer from '../img/Vermeer.jpg'
import Gustav from '../img/Gustav.jpg'
import Arwen from '../img/arwen.jpg'
import Cabanel from '../img/cabanel.jpg'
import Pika from '../img/pika.jpg'
import laNuitEtoilee2 from '../img/laNuitEtoilee2.jpg'
import Caspar from "../img/caspar.jpg"
import Cheval from "../img/cheval.jpg"
import Vague from "../img/vague.jpg"
import Vinci from '../img/Vinci.jpg'
import Ilya from '../img/pouet.jpg'
import Fate from '../img/fate.jpg'
import Fou from "../img/fou.jpg"
import Tarakanova from "../img/Tarakanova.jpg"
import Naruto from "../img/naruto.jpg"
import Ju from "../img/ju.jpg"
import Pdp from "../img/pdp.jpg"
import Vampire from "../img/vampire.jpg"

//pour typer mes questions
interface Question {
  laQuestion: string;
  lesReponses: string[];
  laBonneReponse: string;
  leTableau: string
}

//Objet qui contient mes questions, réponses, bonnes réponses et les images
let mesQuestions: Question[] = [{
  laQuestion: "Qui a peint cette toile ?",
  lesReponses: ["Vincent Van Gogh", "Vincent Von Gogh", "Vincent Van Doom", "Vincent Lagaf'"],
  laBonneReponse: "Vincent Van Gogh",
  leTableau: laNuitEtoilee
}, {
  laQuestion: "Quel est aujourd'hui, exactement le nom de ce tableau ?",
  lesReponses: ["La jeune Fille à la perle", "La jeune Fille au ruban", "La jeune au Turban", "La Mona Lisa d'Europe du Nord"],
  laBonneReponse: "La jeune Fille à la perle",
  leTableau: Vermeer
}, {
  laQuestion: "Quel nom de tableau existe vraiment ?",
  lesReponses: ["Le Baiser", "Le Bisou", "La Bise", "Le Poutou et pas Philippe hein c:"],
  laBonneReponse: "Le Baiser",
  leTableau: Gustav
}, {
  laQuestion: "Quel est le nom de ce magnifique dessin réalisé par moi-même ?",
  lesReponses: ["Arwen les Oreilles Pointues", "Arwen Undómiel", "l'Etoile du Soir", "Liv Tyler"],
  laBonneReponse: "Arwen les Oreilles Pointues",
  leTableau: Arwen
}, {
  laQuestion: "Quel est le nom de l'artiste qui a peint ce tableau ?",
  lesReponses: ["Alexandre Cabanel", "Alexandre Noens", "Alexandre Moreira", "Alexandre Manet"],
  laBonneReponse: "Alexandre Cabanel",
  leTableau: Cabanel
},{
  laQuestion: "Quel est le nom du tableau Original ?",
  lesReponses: ["le Cri", "la Peur", "Coup Critique !", "le Hurlement"],
  laBonneReponse: "le Cri",
  leTableau: Pika
},{
  laQuestion: "Quelle est le nom de la fleur que porte la jolie madame ?",
  lesReponses: ["une Amaranthe", "une Orchidée", "un Camélia", "une Rose"],
  laBonneReponse: "une Amaranthe",
  leTableau: Vampire
}, {
  laQuestion: "Quel est le nom du fleuve représenté sur ce Tableau ?",
  lesReponses: ["le Rhône", "la Meuse", "le Rhin", "la Seine"],
  laBonneReponse: "le Rhône",
  leTableau: laNuitEtoilee2
},{
  laQuestion: "Completez le titre du tableau : La Grande Vague de '...'",
  lesReponses: ["Kanagawa", "Hokusai", "Tsunami", "Mitsubishi"],
  laBonneReponse: "Kanagawa",
  leTableau: Vague
},{
  laQuestion: "Qui est représenté ici ?",
  lesReponses: ["Emma", "Julie", "Clara", "Manon"],
  laBonneReponse: "Emma",
  leTableau: Ju
}, {
  laQuestion: "De quelle couleur est le cheval blanc de Napoléon Bonaparte ?",
  lesReponses: ["blanc", "j'ai pas réfléchi", "j'ai pas lu la question", "j'ai pas regardé le tableau"],
  laBonneReponse: "blanc",
  leTableau: Cheval
}, {
  laQuestion: "Quel est le titre de ce Tableau ?",
  lesReponses: ["Le Voyageur contemplant une mer de nuages", "L'Homme qui regarde par dessus les nuages", "Le Voyageur observant un océan de brumes", "L'Homme qui est au dessus du monde"],
  laBonneReponse: "Le Voyageur contemplant une mer de nuages",
  leTableau: Caspar
},{
  laQuestion: "Quel est le nom de ce Tableau ?",
  lesReponses: ["La Cène", "La Seine", "La Scène", "La Saine"],
  laBonneReponse: "La Cène",
  leTableau: Vinci
},{
  laQuestion: "Qui est représenté ici ?",
  lesReponses: ["Amanda Hummer", "Moi ?", "Toi ?", "Grégoire"],
  laBonneReponse: "Amanda Hummer",
  leTableau: Pdp
}, {
  laQuestion: "Quel est le nom de cet artiste ?",
  lesReponses: ["Ilya Kuvshinov", "Asia Ladowska", "Laura H. Rubin", "Eksandr Hildans"],
  laBonneReponse: "Ilya Kuvshinov",
  leTableau: Ilya
}, {
  laQuestion: "Qui est représenté sur cette superbe toile faite par moi c: ?",
  lesReponses: ["Le Roi Arthur", "Sayaka Miki", "Jeanne d'Arc", "La Jeune fille à l'Epée"],
  laBonneReponse: "Le Roi Arthur",
  leTableau: Fate
}, {
  laQuestion: "Quel est le nom de la série dont cette œuvre est tirée ?",
  lesReponses: ["Le Jeu de la Dame", "Queen's Gambit", "La Regina Degli Scacchi", "クイーンズ・ギャンビット"],
  laBonneReponse: "Queen's Gambit",
  leTableau: Fou
}, {
  laQuestion: "Quelle est l'ouverture tirée du titre de cette œuvre ?",
  lesReponses: ["d4 d5 c4", "e4 e5 f4", "e4 e6 d4", "d4 d5 c5"],
  laBonneReponse: "d4 d5 c4",
  leTableau: Fou
}, {
  laQuestion: "En quelle année Constantin Tretiakov a réalisé : La Princesse Tarakanova",
  lesReponses: ["1864", "1846", "1684", "1648"],
  laBonneReponse: "1864",
  leTableau: Tarakanova
}, {
  laQuestion: "Qui est representé sur cette toile magnifique ?",
  lesReponses: ["畜生道 Chikushōdō", "地獄道 Jigokudō", "餓鬼道 Gakidō", "天道 Tendō"],
  laBonneReponse: "畜生道 Chikushōdō",
  leTableau: Naruto
}
]

//variable pour naviguer entre les questions
let indexQuestion: number = 0;
let score: number = 0;

//selectionne tout les boutons
let reponse = document.querySelectorAll("button");

showQuestion();

/**
 * Pour afficher les questions
 */
function showQuestion() {
  let question = document.querySelector("span");
  let image = document.querySelector("img");
  let bgimg = document.querySelector("body");

  if (indexQuestion < mesQuestions.length) {

    //pour background image
    if (bgimg) {
      bgimg.style.backgroundImage = 'url('+ mesQuestions[indexQuestion].leTableau+')';
    }

    //pour afficher les questions
    if (question) {
      question.innerHTML = mesQuestions[indexQuestion].laQuestion;
    }

    //pour afficher les images
    if (image) {
      image.src = mesQuestions[indexQuestion].leTableau
    }

    //pour faire en sorte que les réponses soit aléatoires
    let test = mesQuestions[indexQuestion].lesReponses
    let tabRandom = test.slice();

    tabRandom.sort(function () {
      return 0.5 - Math.random();
    });

    //je sais plus mais c'est important
    reponse.forEach(function (pouet, index) {
      pouet.innerHTML = tabRandom[index]
      pouet.style.background = "";
      pouet.disabled = false;
      pouet.blur()
    });

  } else {
    //a completer
     if (score == 20) {
      if (question) {
        question.innerHTML = `Vous avez fini ! Votre score est de ${score} / 20 ! Incroyable !`;
      }
      if(image) {
        image.src = 'https://media.tenor.com/bD9vHNiR1rQAAAAd/boom-mind-blown.gif'
      }
      resultat()

    } else if (score >= 15 && score < 20) {
      if (question) {
        question.innerHTML = `Vous avez fini ! Votre score est de ${score} / 20 ! Pas mal !`;
      }
      if(image) {
        image.src = 'https://media.tenor.com/jrIAsC6362EAAAAC/clap-clapping.gif'
      }
      resultat()

    } else if (score >= 10 && score < 15) {
      if (question) {
        question.innerHTML = `Vous avez fini ! Votre score est de ${score} / 20 ! Peu mieux faire ! Courage !`;
      }
      if(image) {
        image.src = 'https://media.tenor.com/KQ_g6uS66bUAAAAC/ganbate-kudasai.gif'
      }
      resultat()

    } else if (score >= 5 && score < 10) {
      if (question) {
        question.innerHTML = `Vous avez fini ! Votre score est de ${score} / 20 ! Bah alors ? Qu'est ce qui c'est passé ?`;
      }
      if(image) {
        image.src = 'https://media.tenor.com/8RARhGNaClQAAAAC/funny-bunny-confused-bunny.gif'
      }
      resultat()

    } else if (score >= 0 && score < 5) {
      if (question) {
        question.innerHTML = `Vous avez fini ! Votre score est de ${score} / 20 ! Est-ce que tout va bien ???`;
      }
      if(image) {
        image.src = 'https://media.tenor.com/HHbDUjAOYE0AAAAC/worried-kermit.gif'
      }
      resultat()

    } else if (score >= -20 && score < 0) {
      if (question) {
        question.innerHTML = `Ne vous en faite pas c'est fini ! Votre score est de ${score} / 20 ! Vous voulez un café ?`;
      }
      if(image) {
        image.src = 'https://i.giphy.com/media/NWg7M1VlT101W/giphy.webp'
      }
      resultat()

    } else if (score >= -39 && score < -20) {
      if (question) {
        question.innerHTML = `Bonk ! Votre score est de ${score} / 20 ! C'est nul !`;
      }
      if(image) {
        image.src = 'https://i.giphy.com/media/30lxTuJueXE7C/giphy.webp'
      }
      resultat()

    } else if (score == -40 ) {
      window.location.href = 'https://shattereddisk.github.io/rickroll/rickroll.mp4';

    } else {
      if (question) {
        question.innerHTML = `Tricheur !`;
      }
      if(image) {
        image.src = 'https://i.giphy.com/media/30lxTuJueXE7C/giphy.webp'
      }
      resultat()
    }

  }

  /**
   * Fonction pour masquer les boutons une fois le quiz terminé
   */
  function resultat() {
    const btn1 = document.querySelector(".btn1");
    const btn2 = document.querySelector(".btn2");
    let section = document.querySelector("section");

    let divRow = document.createElement("div");
    section?.append(divRow);
    divRow.classList.add ("container-fluid")

    //crée un bouton rejouer dans une div
    let form = document.createElement("a");
    divRow?.append(form);
    form.classList.add ("button-6","rejouer")
    form.innerHTML = "Rejouer"
    form.href="index.html"

    if (btn1) {
      btn1.classList.add("d-none")
    }
    if (btn2) {
      btn2.classList.add("d-none")
    }

    if (bgimg) {
      bgimg.style.backgroundImage = ""
      bgimg.style.backgroundColor = 'black'
    }
  }
}

//pour afficher les reponses à la place des boutons
reponse.forEach(function (pouet, index) {

  pouet.addEventListener("click", (event) => {

    //defini la bonne réponse
    let bonneReponse = mesQuestions[indexQuestion].laBonneReponse

    //si la réponse est bonne
    if (pouet.innerHTML == bonneReponse) {
      pouet.style.background = 'green';
      score++;
      indexQuestion++;
      setTimeout(showQuestion, 10);
      console.log("votre score est de :",score);
    }

    //Si la réponse n'est pas bonne
    else if (pouet.innerHTML != bonneReponse) {
      pouet.style.background = 'red';
      pouet.disabled = true;
      score--;          
      console.log("Mauvaise Réponse !", score);
    }
  });
});
