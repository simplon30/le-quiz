# Projet Quiz

## Presentation

Bonjour ! Je vais vous présenter mon quiz.  
Le projet est réalisé dans le cadre de la formation développeur web et web mobile de Simplon.
C'est le deuxième projet que je réalise dans ce cadre, le premier étant le portfolio.  

J'ai utilisé les languages suivants : 
- HTML
- CSS ( Bootstrap )
- Typescript

J'ai aussi utilité Parcel.

Le but est de faire un quiz sur l'art contenant différents éléments :
- Un écran d'accueil avec les règles expliqué avec un bouton pour commencer le quiz.
- Une page contenant une image, une question et quatre propositions de réponse.
- Chaque bonne réponse donnera plus un point et chaque mauvaise réponse fera moins un point, il faudra trouver la bonne réponse pour avancer dans le quiz.
- Une fois la bonne réponse trouvée, les éléments sont remplacés pour que la question d'après s'affiche en utilisant du Typescript.
- Une fois le quiz fini, il y a un écran de score qui en fonction du score obtenu affichera un gif et une phrase différente.
- (Il y a d'ailleurs un easter egg si on arrive à faire un certain score :wink: )

L'idée était de faire quelque chose de simple mais fonctionnel. De mélanger des œuvres d'art que j'aime et certain de mes dessins.

Chaque page est pensée pour prendre 100% de la taille de l'écran. 
Il y a en image de fond le tableau actuellement affiché.

Concernant les questions, elles sont soit très facile soit très difficile et certaines même impossible.
Mais cela fait partie du jeu !

J'ai commenté mon code et mes fonctionus Typescript. 

## wireframe + Maquettes

https://www.figma.com/file/kzrjqOBGJeu88Mh8PxquSe

## Amélioration Futur

- J'aimerais beaucoup qu'il y ait des animations de transition quand on passe d'une question à une autre.
- De même pour l'écran d'accueil, une sorte de slide-down qu'y affiche l'écran des questions.
- Que le responsive soit mieux fait et que les tableaux s'adapte parfaitement aux différentes tailles d'écran.